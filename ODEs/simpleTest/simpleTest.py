import numpy as np
import matplotlib.pyplot as plt

def F(t):
	return np.sin(t)

def f(t,y):
	return -y

def Euler(h,t_0,t_max,y_0):
	N = int((t_max-t_0)/h)
	T,Y = [t_0],[y_0]
	for n in range(N):
		T += [T[n] + h]
		Y += [Y[n] + h*f(T[n],Y[n])]
	return T,Y

h  = 0.01
tD = [0,10]
y_0 = 1

T,Y = Euler(h,tD[0],tD[1],y_0)

plt.plot(T,Y,label="Euler")
plt.plot(T,F(T),label="Exact")
plt.legend()
plt.show()
